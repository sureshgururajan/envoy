image="envoyproxy/envoy-alpine:v1.18-latest"

# Run HTTP Proxy
# ----------------------------------
docker run --rm -it \
  --name app1 \
  -v $(pwd)/app1.json:/app1.json \
  -p 8080:9001 \
  ${image} \
  -c /app1.json
# ----------------------------------





# Run TCP Proxy
# ----------------------------------
# docker run --rm -it \
#   -v $(pwd)/app1-tcp.json:/app1-tcp.json \
#   -p 9001:9001 \
#   ${image} \
#   -c /app1-tcp.json
# ----------------------------------
