const express = require('express')
const app = express()
const port = 8001

app.get('/app1', (req, res) => {
  res.send('Welcome to app1')
})

app.listen(port, () => {
  console.log(`app1 listening at http://localhost:${port}`)
})