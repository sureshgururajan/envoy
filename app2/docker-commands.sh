image="envoyproxy/envoy-alpine:v1.18-latest"

docker run --rm -it \
  --name app2 \
  -v $(pwd)/app2.json:/app2.json \
  -p 8081:9002 \
  ${image} \
  -c /app2.json
