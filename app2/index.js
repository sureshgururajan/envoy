const express = require('express')
const app = express()
const port = 8002

app.get('/app2', (req, res) => {
  res.send('Welcome to app2')
})

app.listen(port, () => {
  console.log(`app2 listening at http://localhost:${port}`)
})