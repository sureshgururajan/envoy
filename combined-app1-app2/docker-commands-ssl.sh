docker build -t envoyproxy-suresh-dev/latest .

image="envoyproxy-suresh-dev/latest"

# Run HTTPS Proxy
# ----------------------------------
docker run --rm -it \
  --name combined-ssl \
  -v $(pwd)/combined-ssl.json:/combined-ssl.json \
  -p 9443:9443 \
  ${image} \
  -c /combined-ssl.json
# ----------------------------------
