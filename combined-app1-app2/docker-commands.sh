image="envoyproxy/envoy-alpine:v1.18-latest"

# Run HTTP Proxy
# ----------------------------------
docker run --rm -it \
  --name combined \
  -v $(pwd)/combined.json:/combined.json \
  -p 9080:9080 \
  ${image} \
  -c /combined.json
# ----------------------------------
