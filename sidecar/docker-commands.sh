image="envoy-nodejs-alpine-dev-sureshg/latest"

docker build -t ${image} .


# NodeJS Docker

docker run --rm -it \
  --name sidecar-nodejsapp \
  -p 3000:3000 \
  ${image}

# Envoy Docker

image="envoyproxy/envoy-alpine:v1.18-latest"

docker run --rm -it \
  --name app1 \
  -v $(pwd)/app1.json:/app1.json \
  -p 9080:9080 \
  ${image} \
  -c /app1.json
